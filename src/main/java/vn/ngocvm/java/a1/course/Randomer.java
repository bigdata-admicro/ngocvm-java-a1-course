/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package vn.ngocvm.java.a1.course;

import java.util.Random;


interface IRandomer{
    public int getRandomNumber(int maxValue);    
}

class UseClassRandom implements IRandomer{
    // Khởi tạo đối tượng 1 lần
    Random rand = new Random();
    
    @Override
    public int getRandomNumber(int maxValue) {
        return rand.nextInt(maxValue);
    }
}

class SystemTimeRandom implements IRandomer{
  
    @Override
    public int getRandomNumber(int maxValue) {
        // Lấy thời gian hệ thống theo mini giây.
        return (int) (System.currentTimeMillis()%maxValue);
    }
}


public class Randomer{
    public static void main(String[] args) {
        IRandomer useClassRandom = new UseClassRandom();
        System.out.println("Random by class: " + useClassRandom.getRandomNumber(100));
        
        IRandomer systemTimeRandom = new SystemTimeRandom();
        System.out.println("Random by systemtime: " + systemTimeRandom.getRandomNumber(100));

        IRandomer systemTimeRandom2 = (int maxValue) -> (int) (System.currentTimeMillis() % maxValue);
        System.out.println("Random by systemtime 2: " + systemTimeRandom2.getRandomNumber(100));
        
        
        // Anonymous Class
        IRandomer anonymusClassRandom = new IRandomer() {
            Random rand = new Random();
            @Override
            public int getRandomNumber(int maxValue) {
                return rand.nextInt(maxValue);
            }
        };
        System.out.println("Random by class: " + anonymusClassRandom.getRandomNumber(100));
    }
}



