/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package vn.ngocvm.java.a1.course;

import java.time.LocalDate;

/**
 *
 * @author User-Admin
 */
public abstract class Employee {
    private int id;
    private String fullName;
    private LocalDate birdDate;

    public Employee(int id, String fullName, LocalDate birdDate) {
        this.id = id;
        this.fullName = fullName;
        this.birdDate = birdDate;
    }

    /**
     * Lấy ra nội dung thỏa thuận quyền lợi mà nhân viên được hưởng.
     * @return nội dung: String
     */
    public abstract String getWorkBenefit();
    
        
    public int getId() {
        return id;
    }

    public String getFullName() {
        return fullName;
    }

    public LocalDate getBirdDate() {
        return birdDate;
    }
}
