/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package vn.ngocvm.java.a1.course;

/**
 *
 * @author User-Admin
 */
public class RandomNumber {

    int value;

    /**
     * Hàm check biến số trong đối tượng hiện tại có phải số nguyên tố không
     *
     * @return 
     * true: nếu đây là số nguyên tố 
     * false: nếu không phải là số nguyên tố
     */
    boolean isPrime() {
        if (value <= 1) {
            return false;
        }
        for (int i = 2; i <= Math.sqrt(value); i++) {
            if (value % i == 0) {
                return false;
            }
        }
        return true;
    }
    // main
    public static void main(String[] args) {
        RandomNumber aNum = new RandomNumber();
        aNum.value = 5;
        if (aNum.isPrime()) {
            System.out.println(aNum.value + " là một số nguyên tố.");
        } else {
            System.out.println(aNum.value + " là không một số nguyên tố.");
            
        }
        
        Thread a = new Thread(()->{
            
        });
    }
}
