/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package vn.ngocvm.java.a1.course.interface_practice;

/**
 *
 * @author User-Admin
 */
public class Dog {
    private final String name;
    private final String color;

    public Dog(String name, String color) {
        this.name = name;
        this.color = color;
    }

    public String getName() {
        return name;
    }

    public String getColor() {
        return color;
    }

    public String print() {
        return "Dog propeties: {" + "name=" + name + ", color=" + color + '}';
    }
}
